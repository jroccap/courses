<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { 
	return view('auth.login');
})->middleware('guest');

Auth::routes(['verify' => false, 'register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('instructors', 'userController@instructors_view')->name('instructors.index');
Route::post('instructors', 'userController@instructors_store')->name('instructors.store');
Route::get('instructors/create', 'userController@instructors_create')->name('instructors.create');
Route::get('instructors/{instructor}/edit', 'userController@instructors_edit')->name('instructors.edit');

Route::get('students', 'userController@students_view')->name('students.index');
Route::get('mycourses', 'courseController@myCourses')->name('mycourses');
Route::post('students', 'userController@students_store')->name('students.store');
Route::get('students/create', 'userController@students_create')->name('students.create');
Route::get('students/{student}/edit', 'userController@students_edit')->name('students.edit');
Route::post('takecourse/{course}', 'userController@takeCourse')->name('takecourse');
Route::post('leavecourse/{course}', 'userController@leaveCourse')->name('leavecourse');

Route::resource('courses', 'courseController')->except('show');
Route::resource('users', 'userController')->only(['update','destroy']);


