<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Course extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'code', 'name', 'description','created_by'
	];

	protected $with=[
		'creator'
	];

	public function scopeBy($query,$instructor){
		return $query->where('created_by','=',$instructor);
	}

	public function users()
	{
		return $this->belongsToMany(User::class)->withTimestamps();
	}

	public function creator()
	{
		return $this->belongsTo('App\User', 'created_by');
	}
}
