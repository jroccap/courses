<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class userController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function validateform(Request $request, $id){

        $request->validate([
            'first_name'=>'required|string|max:40',
            'last_name'=>'required|string|max:40',
            'identification'=>'required|numeric|digits_between:2,15|unique:users'. ($id ? ",identification,$id" : ''),
            'phone'=>'required|numeric|digits_between:4,15',
            'email'=>'required|max:255|string|unique:users'. ($id ? ",email,$id" : '')
        ]);

    }

    //instructors
    public function instructors_view(Request $request)
    {
        $instructors= User::rol(2)->orderBy('first_name','DESC')->get();
        $request->user()->authorizeRole(1);
        /*$users=User::no_sa()->search($request->q)->orderBy('users.id', 'desc')->paginate(8);
        if ($request->ajax()) {
            return view('users/table', compact('users'))->render();
        }*/
        return view('instructors.index', compact('instructors'));
    }

    public function instructors_create(Request $request)
    {
        $request->user()->authorizeRole(1);
        return view('instructors.create');
    }

    public function instructors_store(Request $request)
    {
        $request->user()->authorizeRole(1);
        $this->validateform($request,'');
        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'identification' => $request->get('identification'),
            'phone' => $request->get('phone'),
            'rol' => 2,
            'email'=> $request->get('email'),
            'password'=> Hash::make($request->get('identification')),
        ]);
        $user->save();
        $request->session()->flash('status', __('The registration has been successful'));
        return back();
    }

    public function instructors_edit(Request $request, $id)
    {
        $request->user()->authorizeRole(1);
        $instructor=User::findOrFail($id);
        if ($instructor->rol!=2) {
            abort(404);
        }
        return view('instructors.edit',compact('instructor'));
    }

    //students
    public function students_view(Request $request)
    {
        $request->user()->authorizeRole(2);
        if ($request->user()->rol===1){
            $students= User::rol(3)->orderBy('first_name','DESC')->get();
        }
        if ($request->user()->rol===2){
            $students= User::rol(3)->students($request->user()->id)->orderBy('first_name','DESC')->get();
        }
        /*$users=User::no_sa()->search($request->q)->orderBy('users.id', 'desc')->paginate(8);
        if ($request->ajax()) {
            return view('users/table', compact('users'))->render();
        }*/
        return view('students.index', compact('students'));
    }

    public function students_create(Request $request)
    {
        $request->user()->authorizeRole(1);
        return view('students.create');
    }

    public function students_store(Request $request)
    {
        $request->user()->authorizeRole(1);
        $this->validateform($request,'');
        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'identification' => $request->get('identification'),
            'phone' => $request->get('phone'),
            'rol' => 3,
            'email'=> $request->get('email'),
            'password'=> Hash::make($request->get('identification')),
        ]);
        $user->save();
        $request->session()->flash('status', __('The registration has been successful'));
        return back();
    }

    public function students_edit(Request $request, $id)
    {
        $request->user()->authorizeRole(1);
        $student=User::findOrFail($id);
        if ($student->rol!=3) {
            abort(404);
        }
        return view('students.edit',compact('student'));
    }


    public function takeCourse(Request $request,$id)
    {
        $student=User::findOrFail($request->user()->id);
        if ($request->user()->rol!=3) {
            abort(404);
        }

        $student->courses()->attach($id);
        $request->session()->flash('status', __('The update has been successful'));
        return back();
    }

    public function leaveCourse(Request $request,$id)
    {
        $student=User::findOrFail($request->user()->id);
        if ($request->user()->rol!=3) {
            abort(404);
        }

        $student->courses()->detach($id);
        $request->session()->flash('status', __('The update has been successful'));
        return back();
    }


    public function update(Request $request, $id)
    {
        $request->user()->authorizeRole(1);
        $this->validateform($request,$id);
        $user=User::findOrFail($id);
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->identification = $request->get('identification');
        $user->phone = $request->get('phone');
        $user->email= $request->get('email');
        $user->save();
        $request->session()->flash('status', __('The update has been successful'));
        return back();
    }


    public function destroy(Request $request, $id)
    {
        $request->user()->authorizeRole(1);
        $user=User::findOrFail($id);
        if ($user->rol===3) {
            $user->courses()->detach();
        }
        $user->delete();
        $request->session()->flash('status', __('The elimination has been successful'));
        return back();
    }
}
