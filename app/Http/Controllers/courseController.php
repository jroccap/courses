<?php

namespace App\Http\Controllers;

use App\Course;
use App\User;
use Illuminate\Http\Request;

class courseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function validateform(Request $request, $id){
        if($request->user()->hasRole(1)){
            $request->validate([
                'code'=>'required|string|max:20',
                'name'=>'required|string|max:100',
                'description'=>'required|string',
                'instructor'=>'required',
            ]);
        }
        if($request->user()->rol===2){
            $request->validate([
                'code'=>'required|string|max:20',
                'name'=>'required|string|max:100',
                'description'=>'required|string'
            ]);
        }
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRole(3);
        $courses=Course::all();
        if ($request->user()->rol===2){
            $courses=Course::by($request->user()->id)->get(); 
        }
        return view('courses.index',compact('courses'));
    }


    public function myCourses(Request $request)
    {
        $request->user()->authorizeRole(3);
        if ($request->user()->rol!=3) {
            abort(404);
        }
        $courses=User::rol(3)->find($request->user()->id)->courses;
        return view('courses.mycourses',compact(['courses']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->user()->authorizeRole(2);
        if ($request->user()->rol===1){
            $instructors=User::rol(2)->orderBy('first_name','DESC')->get();
            return view('courses.create',compact(['instructors']));
        }
        return view('courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->user()->authorizeRole(2);
        $this->validateform($request,'');
        if ($request->user()->rol===1){
            $creator=$request->get('instructor');
        }
        if ($request->user()->rol===2){
            $creator=$request->user()->id;
        }
        $course = Course::create([
            'code' => $request->get('code'),
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'created_by' => $creator,
        ]);
        $course->save();
        $request->session()->flash('status', __('The registration has been successful'));
        return back();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $request->user()->authorizeRole(2);
        $course=Course::findOrFail($id);
        if ($request->user()->rol===1){
            $instructors=User::rol(2)->orderBy('first_name','DESC')->get();
            return view('courses.edit',compact(['instructors','course']));
        }
        if ($request->user()->rol===2 && $course->created_by!=$request->user()->id) {
            abort(404);
        }
        return view('courses.edit',compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->user()->authorizeRole(2);
        $this->validateform($request,$id);
        if ($request->user()->rol===1){
            $creator=$request->get('instructor');
        }
        if ($request->user()->rol===2){
            $creator=$request->user()->id;
        }
        $course = Course::findOrFail($id);
        $course->code = $request->get('code');
        $course->name = $request->get('name');
        $course->description = $request->get('description');
        $course->created_by = $creator;
        $course->save();
        $request->session()->flash('status', __('The update has been successful'));
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $request->user()->authorizeRole(1);
        $course=Course::findOrFail($id);
        $course->users()->detach();
        $course->delete();
        $request->session()->flash('status', __('The elimination has been successful'));
        return back();
    }
}
