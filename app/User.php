<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'identification', 'phone', 'rol', 'email', 'password', 'last_login', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function courses()
    {
        return $this->belongsToMany(Course::class)->withTimestamps();
    }

    public function course()
    {
        return $this->hasMany('App\Course');
    }

    public function scopeRol($query,$rol){
        return $query->where('rol','=',$rol);
    }

    public function scopeStudents($query,$instructor){
        return $query->join('course_user', 'users.id', '=', 'course_user.user_id')
        ->join('courses', 'courses.id', '=', 'course_user.course_id')
        ->where('courses.created_by','=',$instructor)
        ->select('users.*')->groupBy('users.id');
    }


    //roles
    public function authorizeRole($rol)
    {
        abort_unless($this->hasRole($rol), 401);
        return true;
    }

    public function hasRole($role)
    {
        if (auth()->user()->rol<=$role) {
            return true;
        }
        return false;
    }

    public function hasCourse($course) {
        return auth()->user()->courses->contains($course);
    }

}
