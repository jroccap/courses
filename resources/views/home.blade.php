@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-center">{{__("Dashboard")}}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <div class="row justify-content-center">
                        @if (auth()->user()->hasRole(1))

                        <div class="col-sm-6 col-md-4">
                            <a href="{{route('instructors.index')}}" class="card shadow-sm text-decoration-none tr text-center text-primary">
                                <div class="card-body">
                                    <div class="display-3">
                                        <i class="fa fa-user-tie ">
                                        </i>
                                    </div>
                                    <span class="fw-mb">
                                        {{__('Instructors') }}
                                    </span>
                                </div>
                            </a>
                        </div>
                        @endif
                        @if (auth()->user()->hasRole(2))
                        <div class="col-sm-6 col-md-4">
                            <a href="{{route('students.index')}}" class="card shadow-sm text-decoration-none tr text-center text-danger">
                                <div class="card-body">
                                    <div class="display-3">
                                        <i class="fa fa-users">
                                        </i>
                                    </div>
                                    <span class="fw-mb">
                                        {{__('Students') }}
                                    </span>
                                </div>
                            </a>
                        </div>
                        @endif
                        <div class="col-sm-6 col-md-4">
                            <a href="{{route('courses.index')}}" class="card shadow-sm text-decoration-none tr text-center text-success">
                                <div class="card-body">
                                    <div class="display-3">
                                        <i class="fa fa-book">
                                        </i>
                                    </div>
                                    <span class="fw-mb">
                                        {{__('Courses') }}
                                    </span>
                                </div>
                            </a>
                        </div>
                        @if (auth()->user()->rol===3)
                        <div class="col-sm-6 col-md-4">
                            <a href="{{route('mycourses')}}" class="card shadow-sm text-decoration-none tr text-center text-info">
                                <div class="card-body">
                                    <div class="display-3">
                                        <i class="fa fa-book-reader">
                                        </i>
                                    </div>
                                    <span class="fw-mb">
                                        {{__('My courses') }}
                                    </span>
                                </div>
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
