@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    {{__("Students")}}
                    <a class="btn btn-secondary float-right btn-sm" href="{{route('home')}}"><i class="fa fa-arrow-left"></i></a>
                    @if (auth()->user()->hasRole(1))
                    <a class="btn btn-primary mr-2 float-right btn-sm" href="{{route('students.create')}}"><i class="fa fa-plus"></i></a>
                    @endif

                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    {{-- <div class="users">
                        @include('students.table')
                    </div> --}}
                    @if(!$students->isEmpty())

                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{__("First name")}}</th>
                                    <th scope="col">{{__("Last name")}}</th>
                                    <th scope="col">{{__("Identification")}}</th>
                                    <th scope="col">{{__("Phone")}}</th>
                                    <th scope="col">{{__("Email")}}</th>
                                    @if (auth()->user()->rol===2)
                                    <th scope="col">{{__("Course")}}</th>
                                    @endif
                                    @if (auth()->user()->hasRole(1))
                                    <th scope="col"></th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($students as $student)
                                <tr>
                                    <td>{{$student['first_name']}}</td>
                                    <td>{{$student['last_name']}}</td>
                                    <td>{{$student['identification']}}</td>
                                    <td>{{$student['phone']}}</td>
                                    <td>{{$student['email']}}</td>
                                    @if (auth()->user()->rol===2)
                                    <td>
                                        @foreach($student->courses as $course)
                                        @if ($course->created_by===auth()->user()->id)
                                        <a href="{{ route('courses.edit',$course->id) }}">{{$course->name}}</a>|
                                        @endif
                                        @endforeach
                                    </td>
                                    @endif
                                    @if (auth()->user()->hasRole(1))
                                    <td>
                                        <a class="btn btn-sm btn-primary" href="{{route('students.edit',$student->id)}}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <form method="POST" action="{{route('users.destroy',$student->id)}}" class="d-inline-block">
                                            @method('DELETE')
                                            <button class="btn-sm btn btn-danger delete" data-placement="left" data-tt="tooltip" title="{{__('Delete company')}}" data-id="{{ $student->id }}" data-ts="{{__('Are you sure?')}}"  data-tx='{{__("You won't be able to revert this!")}}' data-ty="{{__('Yes')}}">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            @csrf
                                        </form>
                                    </td> 
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                    <h5 class="text-center">
                        {{__("No record found")}}
                    </h5>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
