@if(!$users->isEmpty())
<div id="load" class="position-relative">
    <div class="table-responsive mb-2">

        <table class="table table-striped table-sm min-1000 mb-0">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">{{__("Name")}}</th>
                    <th scope="col">{{__("Email")}}</th>
                    <th scope="col">{{__("Tipo")}}</th>
                    <th scope="col" class="text-center">{{__("Creation date")}}</th>
                    <th scope="col" class="text-center">{{__("Update date")}}</th>
                    @if (levelcheck('Super'))
                    <th scope="col" class="text-right">{{__("Actions")}}</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)  
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @foreach ($user->roles->sortByDesc('name') as $rol)
                        @switch($rol->id)
                        @case(2)
                        {{ __("Administrator") }},
                        @break
                        @case(3)
                        {{ __("Storer") }},
                        @break
                        @case(4)
                        {{ __("Buyer") }},
                        @break
                        @default
                        
                        @endswitch
                        @endforeach
                    </td>
                    <td class="text-center">{{ $user->created_at }}</td>
                    <td class="text-center">{{ $user->updated_at }}</td>
                    @if (levelcheck('Super'))
                    <td class="text-right">
                        <a class="btn-sm btn btn-dark" data-placement="left" data-tt="tooltip" title="{{__('Edit user')}}" href="{{ route('users.edit',$user->id) }}">
                            <i class="fa fa-pen"></i>
                        </a>
                        <form method="POST" class="d-inline-block">
                            @method('DELETE')
                            <button class="btn-sm btn btn-danger delete" data-placement="left" data-tt="tooltip" title="{{__('Delete user')}}" data-id="{{ $user->id }}" data-ts="{{__('Are you sure?')}}"  data-tx='{{__("You won't be able to revert this!")}}' data-ty="{{__('Yes')}}">
                                <i class="fa fa-trash"></i>
                            </button>
                            @csrf
                        </form>
                    </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>

<div class="table-responsive">
    {{ $users->onEachSide(2)->links() }}
</div>
@else
<div id="load" class="position-relative">
    <div class="text-center mt-4">
        <p>
            {{__("No record found")}}
        </p>
        @if (levelcheck('Super'))
        <a class="btn  btn-sm btn-primary mt-1" href="{{ route('users.create') }}">
            <i class="fa fa-plus"></i> {{__("Add user")}}
        </a>
        @endif
    </div>
</div>
@endif
