@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    {{__("Courses")}}
                    <a class="btn btn-secondary float-right btn-sm" href="{{route('home')}}"><i class="fa fa-arrow-left"></i></a>
                    @if (auth()->user()->hasRole(2))
                    <a class="btn btn-primary mr-2 float-right btn-sm" href="{{route('courses.create')}}"><i class="fa fa-plus"></i></a>
                    @endif

                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }} 
                        @if (auth()->user()->rol===3) |
                        <a href="{{ route('mycourses') }}">{{__("My courses")}}</a>
                        @endif
                    </div>
                    @endif
                    {{-- <div class="users">
                        @include('courses.table')
                    </div> --}}
                    @if(!$courses->isEmpty())
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{__("Name")}}</th>
                                    <th scope="col">{{__("Code")}}</th>
                                    <th scope="col">{{__("Description")}}</th>
                                    <th scope="col">{{__("Instructor")}}</th>
                                    @if (auth()->user()->hasRole(3))
                                    <th scope="col"></th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($courses as $course)
                                <tr>
                                    <td>{{$course['name']}}</td>
                                    <td>{{$course['code']}}</td>
                                    <td>{{$course['description']}}</td>
                                    <td>{{$course->creator->first_name.' '.$course->creator->last_name}}</td>
                                    @if (auth()->user()->hasRole(2))
                                    <td>
                                        <a class="btn btn-sm btn-primary" href="{{route('courses.edit',$course->id)}}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        @if (auth()->user()->hasRole(1))
                                        <form method="POST" action="{{route('courses.destroy',$course->id)}}" class="d-inline-block">
                                            @method('DELETE')
                                            <button class="btn-sm btn btn-danger delete" data-placement="left" data-tt="tooltip" title="{{__('Delete company')}}" data-id="{{ $course->id }}" data-ts="{{__('Are you sure?')}}"  data-tx='{{__("You won't be able to revert this!")}}' data-ty="{{__('Yes')}}">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            @csrf
                                        </form>
                                        @endif
                                    </td> 
                                    @endif
                                    @if (auth()->user()->rol===3)
                                    @if (auth()->user()->hasCourse($course->id))
                                    <td>
                                        <form method="POST" action="{{route('leavecourse',$course->id)}}" class="d-inline-block">
                                            <button class="btn-sm btn btn-danger"><i class="fa fa-sign-out-alt"></i> {{__('Leave the course')}}</button>
                                            @csrf
                                        </form>
                                    </td> 
                                    @else
                                    <td>
                                        <form method="POST" action="{{route('takecourse',$course->id)}}" class="d-inline-block">
                                            <button class="btn-sm btn btn-success"><i class="fa fa-sign-in-alt"></i> {{__('Enter the course')}}</button>
                                            @csrf
                                        </form>
                                    </td> 
                                    @endif
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                    <h5 class="text-center">
                        {{__("No record found")}}
                    </h5>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
