@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{__("Courses")}}
                    <a class="btn btn-secondary float-right btn-sm" href="{{route('courses.index')}}"><i class="fa fa-arrow-left"></i></a>

                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form method="POST" action="{{ route('courses.store') }}" class="needs-validation" novalidate>
                        @csrf
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="code">{{ __("Code") }}</label>
                                <input type="text" class="form-control @error('code') is-invalid @enderror" name="code" id="code" placeholder="{{__('Code') }}" value="{{ old('code') }}" required>
                                @error('code')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('code') }}</strong>
                                </div>
                                @enderror
                                <div class="invalid-feedback">
                                    {{__("Please enter your ")." ".Str::lower(__("Code"))}}
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="name">{{__("Name") }}</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="{{__('Name') }}" value="{{ old('name') }}" required>

                                @error('name')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </div>
                                @enderror
                                <div class="invalid-feedback">
                                    {{__("Please enter your ")." ".Str::lower(__("Name"))}}
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="description">{{__("Description") }}</label>
                                <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" placeholder="{{__('Description') }}"  required>{{ old('description') }}</textarea>
                                @error('description')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </div>
                                @enderror
                                <div class="invalid-feedback">
                                    {{__("Please enter your ")." ".Str::lower(__("Description"))}}
                                </div>
                            </div>
                            @if (auth()->user()->hasRole(1))
                            <div class="col-md-12 mb-3">
                                <label for="instructor">{{__("Instructor") }}</label>
                                <select class="custom-select @error('instructor') is-invalid @enderror" name="instructor" id="instructor" required>
                                    <option value="" selected>{{__("Select an instructor")}}</option>
                                    @foreach($instructors as $instructor)
                                    <option {{ old('instructor') == $instructor->id ? 'selected' : '' }} value="{{$instructor->id}}">
                                        {{$instructor->first_name.' '.$instructor->last_name}}
                                    </option>
                                    @endforeach
                                </select>
                                @error('instructor')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('instructor') }}</strong>
                                </div>
                                @enderror
                                <div class="invalid-feedback">  
                                    {{__("Please enter your ")." ".Str::lower(__("instructor"))}}
                                </div>
                            </div>
                            @endif
                        </div>
                        <hr>
                        <button class="btn btn-secondary" type="reset">{{__("Restart")}}</button>
                        <button class="btn btn-primary float-right" type="submit">{{__("Save")}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
