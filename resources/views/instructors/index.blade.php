@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    {{__("Instructors")}}
                    <a class="btn btn-secondary float-right btn-sm" href="{{route('home')}}"><i class="fa fa-arrow-left"></i></a>
                    <a class="btn btn-primary mr-2 float-right btn-sm" href="{{route('instructors.create')}}"><i class="fa fa-plus"></i></a>

                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    {{-- <div class="users">
                        @include('instructors.table')
                    </div> --}}
                    @if(!$instructors->isEmpty())
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{__("First name")}}</th>
                                    <th scope="col">{{__("Last name")}}</th>
                                    <th scope="col">{{__("Identification")}}</th>
                                    <th scope="col">{{__("Phone")}}</th>
                                    <th scope="col">{{__("Email")}}</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($instructors as $instructor)
                                <tr>
                                    <td>{{$instructor['first_name']}}</td>
                                    <td>{{$instructor['last_name']}}</td>
                                    <td>{{$instructor['identification']}}</td>
                                    <td>{{$instructor['phone']}}</td>
                                    <td>{{$instructor['email']}}</td>
                                    <td>
                                        <a class="btn btn-sm btn-primary" href="{{route('instructors.edit',$instructor->id)}}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <form method="POST" action="{{route('users.destroy',$instructor->id)}}" class="d-inline-block">
                                            @method('DELETE')
                                            <button class="btn-sm btn btn-danger delete" data-placement="left" data-tt="tooltip" title="{{__('Delete company')}}" data-id="{{ $instructor->id }}" data-ts="{{__('Are you sure?')}}"  data-tx='{{__("You won't be able to revert this!")}}' data-ty="{{__('Yes')}}">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            @csrf
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                    <h5 class="text-center">
                        {{__("No record found")}}
                    </h5>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
