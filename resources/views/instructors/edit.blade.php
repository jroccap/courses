@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{__("Instructors")}}
                    <a class="btn btn-secondary float-right btn-sm" href="{{route('instructors.index')}}"><i class="fa fa-arrow-left"></i></a>

                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form method="POST" action="{{ route('users.update',$instructor['id']) }}" class="needs-validation" novalidate>
                        @method('PUT')
                        @csrf
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="first_name">{{ __("First name") }}</label>
                                <input type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" id="first_name" placeholder="{{__('First name') }}" value="{{ $instructor['first_name'] }}" required>
                                @error('first_name')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </div>
                                @enderror
                                <div class="invalid-feedback">
                                    {{__("Please enter your ")." ".strtolower(__("First name"))}}
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="last_name">{{__("Last name") }}</label>
                                <input type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" id="last_name" placeholder="{{__('Last name') }}" value="{{ $instructor['last_name'] }}" required>

                                @error('last_name')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </div>
                                @enderror
                                <div class="invalid-feedback">
                                    {{__("Please enter your ")." ".Str::lower(__("Last name"))}}
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="identification">{{__("Identification") }}</label>
                                <input type="text" class="form-control @error('identification') is-invalid @enderror" name="identification" id="identification" placeholder="{{__('Identification') }}" value="{{ $instructor['identification'] }}" required>
                                @error('identification')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('identification') }}</strong>
                                </div>
                                @enderror
                                <div class="invalid-feedback">
                                    {{__("Please enter your ")." ".Str::lower(__("Identification"))}}
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="phone">{{__("Phone") }}</label>
                                <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="phone" placeholder="{{__('Phone') }}" value="{{ $instructor['phone'] }}" required>
                                @error('phone')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </div>
                                @enderror
                                <div class="invalid-feedback">
                                    {{__("Please enter your ")." ".Str::lower(__("Phone"))}}
                                </div>
                            </div>
                            <div class="col-md-8 mb-3">
                                <label for="email">{{__("Email") }}</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="{{__('Email') }}" value="{{ $instructor['email'] }}" required>
                                @error('email')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </div>
                                @enderror
                                <div class="invalid-feedback">
                                    {{__("Please enter your ")." ".Str::lower(__("Email"))}}
                                </div>
                            </div>
                        </div>
                        <hr>
                        <button class="btn btn-secondary" type="reset">{{__("Restart")}}</button>
                        <button class="btn btn-primary float-right" type="submit">{{__("Update")}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
